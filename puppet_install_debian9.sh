#!/bin/bash

PuppetMasterIP=""
PuppetMasterHostname=""
NodeName=""
DomainName=""
NodeFQDN=""
Timezone=""
Environment=""
RunInterval="60m"
UseSrvRecords=0
SrvDomain=""
CsrPreSharedKey=""

PuppetRepoFilename="puppet6-release-stretch.deb"
PuppetRepoUrl="https://apt.puppetlabs.com/$PuppetRepoFilename"
PuppetConfigBase="/etc/puppetlabs/puppet"
PuppetConfigFile="$PuppetConfigBase/puppet.conf"
PuppetCSR="$PuppetConfigBase/csr_attributes.yaml"

echo "Getting arguments..."

while getopts ":n:i:z:d:e:s:r:t:p:u" opt; do
    case ${opt} in
        d)
            DomainName=$OPTARG
            ;;
        e)
            Environment=$OPTARG
            ;;
        i)
            PuppetMasterIP=$OPTARG
            ;;
        n)
            NodeName=$OPTARG
            ;;
        p)
            CsrPreSharedKey=$OPTARG
            ;;
        r)
            RunInterval=$OPTARG
            ;;
        s)
            PuppetMasterHostname=$OPTARG
            ;;            
        t)
            SrvDomain=$OPTARG
            ;;
        u)            
            UseSrvRecords=1
            ;;
        z)
            Timezone=$OPTARG
            ;;
    esac
done

echo ""
echo "--------------------------"
echo "Puppetmaster IP: $PuppetMasterIP"
echo "Puppetmaster hostname: $PuppetMasterHostname"
echo "Node name: $NodeName"
echo "Domain name: $DomainName"
echo "Timezone: $Timezone"
echo "Environment: $Environment"
echo "Run interval: $RunInterval"
if [ $UseSrvRecords != 0 ]; then
    echo "Using SRV records. Domain: $SrvDomain"
else
    echo "Not using SRV records."
fi
echo "--------------------------"
echo ""

display_usage_information () {
    echo "Usage: "
    echo ""
    echo "  installpuppet -n <node name> -i <puppetmaster IP address> "
    echo "      -s <puppetmaster hostname> -d <domain name> -e <environment> "
    echo "      -z <timezone> [-r <run interval>] [-u] [-t <SRV domain>] "
    echo "      [-p <CSR PreSharedKey>]"
    echo ""
    echo "Required switches: "
    echo ""
    echo "  -d          Domain name for the agent node"
    echo "  -e          The name of the puppet environment"
    echo "  -i          IP Address of the puppet master"
    echo "  -n          Node name (without the domain name)"
    echo "  -s          Puppet master FQDN"
    echo "  -z          The timezone that the node resides in. Default is Etc/UTC "
    echo "              and for Melbourne (AU) the value is Australia/Melbourne"
    echo ""
    echo "Optional switches: "
    echo ""
    echo "  -p          The pre-shared key (or password) to use creating the CSR"
    echo "  -r          The interval between each puppet agent run. Default is 60m"
    echo "  -t          The domain to use when using SRV records"
    echo "  -u          Use SRV records to locate the correct puppet master"
    echo ""
    echo "Examples: "
    echo "  sudo bash installpuppet -n charles -i 10.92.106.212 -s master.royalinternal.local "
    echo "      -d royalinternal.local -e production_servers -t Europe/London -r 30m"
    echo ""
    echo "  This will configure the node with charles.royalinternal.local to use the production_servers "
    echo "  environment and to use the puppet master located at master.royalinternal.local."
    echo ""
    echo "  sudo bash installpuppet -n db003 -i 172.22.0.1 -s puppetmaster01.thisuniversity.edu "
    echo "      -d thisuniversity.edu -e database_servers -t Australia/Melbourne "
    echo "      -u -t puppetservers.local"
    echo ""
    echo "  This will configure the node db003.thisuniversity.edu to use SRV records to locate the "
    echo "  appropriate puppet master, using the domain puppetservers.local."

    exit 1
}

check_permission() {
    if [ `/usr/bin/id -u` != 0 ] ; then
        echo "This must be run as root."
        exit 1
    fi
}

check_arguments() {
    # Mandatory parameters:
    # Puppetmaster IP and hostname
    # Node name
    # Domain name
    # Timezone
    # Environment

    if [ -z $PuppetMasterIP ] || [ -z $PuppetMasterHostname ] || [ -z $NodeName ]  ||
        [ -z $DomainName ] || [ -z $Timezone ] || [ -z $Environment ]; then
        echo "One or more arguments are missing."
        display_usage_information
        exit 1
    fi

    # Optional parameters:
    # Run interval
    # Use SRV records
    # SRV domain

     # If the SRV option is set, make sure the domain is set
    if [ $UseSrvRecords != 0 ] && [ -z $SrvDomain ]; then
        echo "No domain was specified for SRV records. Run this script with --help for full usage information."
        exit 1
    fi
    
}

set_timezone() {
    echo -e "\e[32mSetting timezone..\e[0m"
    timedatectl set-timezone "$Timezone"
    dpkg-reconfigure --frontend noninteractive tzdata
}

check_existing_installation() {
    if [[ -d "/opt/puppetlabs" ]]
    then
        echo "Puppet is already installed."
        exit 1
    fi
}

set_hosts() {
    echo -e "\e[32mUpdating /etc/hosts\e[0m"
    echo "127.0.0.1  localhost" > /etc/hosts
    echo "127.0.0.1  $NodeFQDN" >> /etc/hosts
    echo "$PuppetMasterIP  $PuppetMasterHostname" >> /etc/hosts
}

download_install_puppet() {
    echo ""
    echo -e "\e[32mDownloading puppet\e[0m"
    cd /tmp
    wget $PuppetRepoUrl
    dpkg -i $PuppetRepoFilename
    echo ""
    echo -e "\e[32mUpdating packages\e[0m"
    apt update

    echo ""
    echo -e "\e[32mInstalling puppet\e[0m"
    apt install puppet-agent

    echo ""
    echo -e "\e[32mBlocking the puppet agent from running before config is set\e[0m"
    /opt/puppetlabs/bin/puppet resource service puppet ensure=stopped
}

write_config_file() {
    echo ""
    echo -e "\e[32mWriting puppet configuration file\e[0m"

    mkdir -p $PuppetConfigBase
    touch $PuppetConfigFile

    echo "[agent]" > $PuppetConfigFile
    echo "certname = $NodeFQDN" >> $PuppetConfigFile
    echo "environment = $Environment" >> $PuppetConfigFile
    echo "runinterval = $RunInterval" >> $PuppetConfigFile
    if [ "$UseSrvRecords" != 0 ]; then
        echo "use_srv_records = true" >> $PuppetConfigFile
        echo "srv_domain = $SrvDomain" >> $PuppetConfigFile
    else
        echo "server = $PuppetMasterHostname" >> $PuppetConfigFile
    fi
}

write_csr() {
    if [[ ! -z $CsrPreSharedKey ]]; then
        echo ""
        echo -e "\e[32mWriting puppet CSR\e[0m"

        touch $PuppetCSR
        echo "---" > $PuppetCSR
        echo "custom_attributes:" >> $PuppetCSR        
        echo "  1.2.840.113549.1.9.7: \"$CsrPreSharedKey\"" >> $PuppetCSR
    fi
}

request_certificate() {
    echo ""
    echo -e "\e[32mRequesting a certificate from the puppet master and performing first-run\e[0m"
    /opt/puppetlabs/bin/puppet agent --test --waitforcert 30
}

enable_puppet_agent() {
    echo ""
    echo "\e[32mEnabling the puppet agent\e[0m"
    /opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true
}

check_permission
check_arguments
set_timezone
check_existing_installation
NodeFQDN="$NodeName.$DomainName"
set_hosts
download_install_puppet
write_config_file
write_csr
request_certificate
enable_puppet_agent

echo ""
echo -e "\e[32m[Done]"
